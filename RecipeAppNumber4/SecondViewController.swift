//
//  SecondViewController.swift
//  RecipeAppNumber4
//
//  Created by Matt DeBoer on 2/2/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    var object:Dictionary = [String:Any]()
    
    @IBOutlet var textView1: UITextView!
    @IBOutlet var textView: UITextView!
    @IBOutlet var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let url = URL(string: object["image"] as! String)
        let data = NSData(contentsOf: url!)
        imageView.image = UIImage(data: data! as Data, scale: 1.0)// Error here
        
//        let url = URL(string: object["image"] as! String)
//        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//        imageView.image = UIImage(data: data!)
        
        self.textView.text = object["ing"] as! String
        self.textView1.text = object["dir"] as! String
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

