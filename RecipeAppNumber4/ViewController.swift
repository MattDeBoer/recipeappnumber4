//
//  ViewController.swift
//  RecipeAppNumber4
//
//  Created by Matt DeBoer on 2/2/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    
    var arrayOfData = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let path = Bundle.main.path(forResource: "Property List", ofType: "plist") {
        
            //If your plist contain root as Array
                
                if let contentOfPlist = NSArray.init(contentsOfFile: path ) {
                arrayOfData = contentOfPlist as! [[String: Any]]
                tableView.reloadData()
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        let object = arrayOfData[indexPath.row]
        cell?.textLabel?.text = (object["title"] as! String)
        let url = URL(string: object["image"] as! String)
        let data = NSData(contentsOf: url!)
        cell?.imageView?.image = UIImage(data: data! as Data)
        
        return cell!

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "makingTrans", sender: arrayOfData[indexPath.row])
//        let vc = SecondViewController()
//        vc.object = arrayOfData[indexPath.row]
//        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnClicked(_ sender: Any) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let secondVC = segue.destination as! SecondViewController
        secondVC.object = sender as! Dictionary<String, Any>
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
